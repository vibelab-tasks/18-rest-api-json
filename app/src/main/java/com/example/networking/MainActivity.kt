package com.example.networking

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://jsonplaceholder.typicode.com/"
const val EDIT_MESSAGE_CODE = 1

class MainActivity : AppCompatActivity() {

    lateinit var msgAdapter: MessageListAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    lateinit var rvMessages: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvMessages = findViewById<RecyclerView>(R.id.rvMessages)

        linearLayoutManager = LinearLayoutManager(this)
        rvMessages.layoutManager = linearLayoutManager

        getMessages()
    }

    fun getMessages() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MessageDataItem>?> {
            override fun onResponse(call: Call<List<MessageDataItem>?>, response: Response<List<MessageDataItem>?>) {
                val responseBody = response.body()!!
                msgAdapter = MessageListAdapter(baseContext, responseBody)
                msgAdapter.onItemClick = { message ->
                    val intent = Intent(this@MainActivity, EditMessageActivity::class.java)
                    intent.putExtra("message", message)
                    startActivity(intent)

                    println("debug: onClick: ${message}")
                }
                msgAdapter.notifyDataSetChanged()

                rvMessages.adapter = msgAdapter

            }

            override fun onFailure(call: Call<List<MessageDataItem>?>, t: Throwable) {
                println("debug: onFailure: ${t.message}")
            }
        })
    }
}