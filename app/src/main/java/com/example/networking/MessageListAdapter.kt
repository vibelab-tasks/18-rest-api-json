package com.example.networking

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MessageListAdapter( val context: Context, val msgList: List<MessageDataItem>):
    RecyclerView.Adapter<MessageListAdapter.ViewHolder>()
{

    var onItemClick: ((MessageDataItem) -> Unit) = {}

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var tvUserIdCardLabel: TextView
        var tvPostIdCardLabel: TextView
        var tvTitleCardLabel: TextView
        var tvBodyCardLabel: TextView

        init {
            tvBodyCardLabel = itemView.findViewById(R.id.tvBodyCardLabel)
            tvTitleCardLabel = itemView.findViewById(R.id.tvTitleCardLabel)
            tvPostIdCardLabel = itemView.findViewById(R.id.tvPostIdCardLabel)
            tvUserIdCardLabel = itemView.findViewById(R.id.tvUserIdCardLabel)

            itemView.setOnClickListener {
                onItemClick(msgList[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.messagelist_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvUserIdCardLabel.text = "user#${msgList[position].userId}"
        holder.tvPostIdCardLabel.text = "post#${msgList[position].id.toString()}"
        holder.tvTitleCardLabel.text = msgList[position].title
        holder.tvBodyCardLabel.text = msgList[position].body
    }

    override fun getItemCount(): Int = msgList.size
}