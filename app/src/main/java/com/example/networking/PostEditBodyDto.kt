package com.example.networking

data class PostEditBodyDto(
    val userId: Int,
    val title: String,
    val body: String
)

data class PostEditResponseDto (
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)