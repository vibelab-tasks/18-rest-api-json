package com.example.networking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class EditMessageActivity : AppCompatActivity() {

    lateinit var tvUserIdEditLabel: TextView
    lateinit var tvPostIdEditLabel: TextView
    lateinit var etTitle: EditText
    lateinit var etBody: EditText
    lateinit var btnEdit: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_message)

        tvUserIdEditLabel = findViewById(R.id.tvUserIdEditLabel)
        tvPostIdEditLabel = findViewById(R.id.tvPostIdEditLabel)
        etTitle = findViewById(R.id.etTitle)
        etBody = findViewById(R.id.etBody)
        btnEdit = findViewById(R.id.btnEdit)

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val message = intent.getSerializableExtra("message") as MessageDataItem

        with(message) {
            tvUserIdEditLabel.text = "user#"+userId.toString()
            tvPostIdEditLabel.text = "post#"+id.toString()
            etTitle.setText(title)
            etBody.setText(body)
        }

        btnEdit.setOnClickListener {

            val data = PostEditBodyDto(
                userId=message.userId, title=etTitle.text.toString(), body=etBody.text.toString()
            )
            val putData = retrofitBuilder.editPost(postId=message.id, body=data)

            putData.enqueue(object : Callback<PostEditResponseDto?> {
                override fun onResponse(call: Call<PostEditResponseDto?>, response: Response<PostEditResponseDto?>) {
                    val data = response.body()

                    println("debug: edit post, $data")
                    finish()
                }

                override fun onFailure(call: Call<PostEditResponseDto?>, t: Throwable) {
                    println("debug: onFailure: ${t.message}")
                }
            })

        }
    }
}