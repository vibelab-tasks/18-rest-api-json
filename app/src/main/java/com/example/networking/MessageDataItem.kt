package com.example.networking

data class MessageDataItem (
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
) : java.io.Serializable