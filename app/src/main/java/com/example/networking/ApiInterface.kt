package com.example.networking

import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @GET("posts")
    fun getData(): Call<List<MessageDataItem>>

    @PUT("posts/{postId}")
    fun editPost(
        @Path("postId") postId: Int,
        @Body body: PostEditBodyDto
    ): Call<PostEditResponseDto>
}